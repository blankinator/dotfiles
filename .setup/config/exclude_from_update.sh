# read list of packages to exclude from update
exclude_list_file=~/.setup/packages/exclude_from_updating.list
exclude_list=$(cat $exclude_list_file)

pacman_config_file=/etc/pacman.conf

# get current IngorePkg line from pacman.conf
# line may have # at the beginning
old_ignore_line=$(grep -P "^#?IgnorePkg" $pacman_config_file)
echo old_ignore_line: $old_ignore_line

# split line at = and take second part
old_packages_line=$(echo $old_ignore_line | cut -d= -f2)
echo old_packages_line: $old_packages_line

# extract list of packages from line, separated by spaces
old_ignore_packages=$(echo $old_packages_line| tr -s ' ' '\n')
echo old_packages: $old_packages

# add packages to ignore line, if not already there
new_ignore_packages=$old_ignore_packages
for package in $exclude_list; do
    if [[ ! $old_ignore_packages =~ $package ]]; then
	new_ignore_packages="$new_ignore_packages $package"
    fi
done

# join packages with spaces
new_ignore_packages_line=$(echo $new_ignore_packages | tr '\n' ' ')
echo new_ignore_packages_line: $new_ignore_packages_line

# add IgnorePkg= to beginning of line
new_ignore_line="IgnorePkg	= $new_ignore_packages_line"
echo new_ignore_line: $new_ignore_line

# replace old IgnorePkg line with new one
echo "updating pacman.conf"
sudo sed -i "s/$old_ignore_line/$new_ignore_line/" $pacman_config_file

echo "done"
