#!/bin/bash

config_file_name="30-touchpad.conf"
config_folder="/etc/X11/xorg.conf.d/"
resources_folder="$(dirname "$0")/resources"


if [ ! -f "$config_folder/$config_file_name" ]; then
  if [ -f "$resources_folder/$config_file_name" ]; then
    cp "$resources_folder/$config_file_name" "$config_folder/$config_file_name"
    if [ -f "$config_folder/$config_file_name" ]; then
      echo "File copied successfully."
    else
      echo "Error copying file"
    fi
  else
    echo "Error: Resource file not found."
  fi
else
  echo "File already exists."
fi

