# script for updating all packages but excluding a list of packages

# set path to list of packages to exclude
exclude_list_file=~/.setup/packages/exclude_from_updating.list

# load list of packages to exclude
exclude_list=$(cat $exclude_list_file)

# define function that calls yay update and excludes packages from exclude_list

function update_system {
    yay -Syu --noconfirm --needed --noprovides --ignore $exclude_list
}

# also define function that only updates packages from pacman
function update_pacman_packages {
    sudo pacman -Syu --noconfirm --needed --noprovides --ignore $exclude_list
}

