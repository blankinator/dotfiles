# network utility functions

# get the ip address of the host
function get_local_ip() { echo -e "The interfaces and IPs are:\n$(ip -o -4 addr show | awk '/inet 192.168|inet 10.|inet 172.16/ {split($4, a, "/"); print $2 ": " a[1] "\n"}')"
}

function get_public_ip() {
		public_ip=$(curl -s canhazip.com)
		echo -e "The public IP is: $public_ip"
}

function is_dns_working() {
		dns_working=$(ping -4 -c 1 google.com | grep "1 received" | wc -l)
		if [ $dns_working -eq 1 ]; then
				return 0
		else
				return 1
		fi

}


function reconnect_vpn_on_dns_failure() {
		# reconnect vpn if dns is not working
		if ! is_dns_working; then
				echo "DNS is not working, reconnecting VPN"
				mullvad reconnect
				echo $(mullvad status)
				
		else
				echo "DNS is working, no need to reconnect VPN"
		fi
}

function check_internet_speed() {
		# check if user has permissions to /var/log
		if [ ! -w /var/log ]; then
				echo "Permissions to /var/log are not available"
				echo "Resorting to home directory for log file"
				logfile=~/speedtest.log
		else
				echo "Permissions to /var/log are available"
				logfile=/var/log/speedtest.log
		fi

		# make sure dns is working properly
		reconnect_vpn_on_dns_failure
		
		# add time and date to logfile
		echo -e "\n\n$(date)" >> $logfile

		echo "Running speedtest"
		SpeedTest | tee -a $logfile
		echo "Speedtest complete"
}

