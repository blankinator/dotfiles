#!/bin/bash

function is_git {

		# check if current folder is git repository
		git_check_command=$(git -C $1 rev-parse > /dev/null 2>&1)
	if [[ $? != 0  ]]; then
			return 0
	fi

	return 1
}

function get_current_branch {
	echo $(git branch | grep "*" | awk '{print $NF}')
}

function get_auto_save_branches {

	auto_save_branches_raw=`git branch | grep "git_auto_save" | tail -1 | awk '{print $FN}'`
	auto_save_branches=()
	for i in "${$auto_save_branches_raw[@]}"
	do
		echo ${git_auto_save_branches_raw[i]}
		#auto_save_branches+=${git_auto_save[i]}
	done

	echo $auto_save_branches

}

function git_auto_save {
		
	if [[ $(is_git ".") -ne 0 ]]; then
			echo "Current dir is not a git repository."
			return 1
	fi

	current_branch_name=$(get_current_branch)

	timestamp=`date +%Y_%m_%d_%H_%M_%S`
	new_branch_name="git_auto_save/$HOST/$timestamp"
	
	# check if there is unsaved work
	changes=`git status --ignore-submodules=dirty | grep -E 'modified|deleted|new'`
	if [[ ! $changes ]]; then
			echo "No changes found."
			return
	fi

	# stash changes
	echo "Stashing changes..."
	git stash > /dev/null 2>&1

	# create new branch
	echo "Creating new branch $new_branch_name..."
	git checkout -b $new_branch_name > /dev/null 2>&1
	git push origin $new_branch_name > /dev/null 2>&1

	# apply stashed changes
	echo "Applying stash to new branch..."
	git stash apply > /dev/null 2>&1

	# add changed files
	echo "Adding new/changed files..."
	git add -u > /dev/null 2>&1

	# commit changes
	echo "Commiting changes..."
	git commit -m "git autosave from $timestamp" > /dev/null 2>&1

	# push changes
	echo "Pushing changes..."
	git push --set-upstream origin $new_branch_name > /dev/null 2>&1

	# move back to initial branch
	echo "Moving back to previous branch..."
	git checkout $current_branch_name > /dev/null 2>&1
	
	echo "Done."
}

function remove_auto_save {

	if [[ $(is_git ".") -ne 0 ]]; then
			echo "Current dir is not a git repository."
			return 1
	fi

	echo "Looking for auto-save branches..."
	
	if [[ $1 ]]; then
		auto_save_branch=$1
	else
		auto_save_branch=`git branch | grep "git_auto_save" | head -1 | awk '{print $FN}'`
	fi


	if [[ ! $auto_save_branch ]]; then
			echo "No auto-save branch found."
			return 1
	fi

	auto_save_branch=${auto_save_branch:2}
	echo "Auto-save branch found: $auto_save_branch"

	current_branch=$(get_current_branch)

	# if on autosave branch, move to master
	if [[ $current_branch == $auto_save_branch ]]; then
			echo "Currently on autosave path, moving to Master"
			git checkout master
	fi

	# remove local branch
	echo "Deleting local branch..."
	git branch -d $auto_save_branch > /dev/null 2>&1

	# remove remote branch
	echo "Deleting remote branch..."
	git push origin --delete $auto_save_branch > /dev/null 2>&1

	return 0

}

function remove_all_auto_saves {

	if [[ $(is_git ".") -ne 0 ]]; then
		echo "Current dir is not a git repository."
		return 1
	fi

	if [[ $1 != "y" ]]; then
			echo "Please confirm with 'y'"
			return	
	fi

	# remove all auto save branches
	while [[ 1 ]]; do
		if ! remove_auto_save; then
			break
		fi
		echo "Auto-Save removed."
	done

	echo "All auto-saves removed."
		
}

function load_last_auto_save {
	auto_save_branches=$(get_auto_save_branches)
	echo $auto_save_branches
}
