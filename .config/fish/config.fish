if status is-interactive
    # Commands to run in interactive sessions can go here
  # source all files in the scripts directory
  for file in $HOME/.scripts/*
    source $file
  end
end
