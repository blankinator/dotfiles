" Check if vim-plug is not installed
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    " Display a message to the user
    echo "Installing vim-plug..."
    " Run the installation command
    call system('sh -c ''curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim''')
    " Display a message to inform the user that the installation has completed
    echo "vim-plug installed successfully. Please restart Neovim."
endif

call plug#begin('~/.config/nvim/plugged')

" Add your plugins here, for example:
Plug 'preservim/nerdtree'
Plug 'github/copilot.vim'

call plug#end()


" set tab to 4 spaces for neovim
set tabstop=4
